import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_CARD_LIST]: (store, payload) => {
        return axios.get(apiUrls.DO_CARD_LIST.replace('{memberId}', payload.memberId))
    },
    [Constants.DO_CARD_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_CARD_DETAIL.replace('{id}', payload.id))
    },
}
