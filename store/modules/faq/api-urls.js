const BASE_URL = '/v1/faq'

export default {
    DO_FAQ_LIST: `${BASE_URL}/list/admin`, //get 문의사항 조회
    DO_FAQ_LIST_PAGING: `${BASE_URL}/all/{pageNum} `, //get 문의사항 페이징
    DO_FAQ_DETAIL: `${BASE_URL}/detail/{id}`, //get 문의사항 상세 조회
    DO_FAQ_UPDATE: `${BASE_URL}/comment/{id}`, //put 문의사항 수정
    DO_FAQ_DELETE: `${BASE_URL}/{id}`, //del
}
