import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_FAQ_LIST]: (store) => {
        return axios.get(apiUrls.DO_FAQ_LIST)
    },
    [Constants.DO_FAQ_LIST_PAGING]: (store, payload) => {
        return axios.get(apiUrls.DO_FAQ_LIST_PAGING.replace('{pageNum}', payload.pageNum) )
    },
    [Constants.DO_FAQ_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_FAQ_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_FAQ_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_FAQ_UPDATE.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_FAQ_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_FAQ_DELETE.replace('{id}', payload.id))
    },

}
