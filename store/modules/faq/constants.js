export default {
    DO_FAQ_LIST: 'faq/doFaqList',
    DO_FAQ_LIST_PAGING: 'faq/doFaqListPaging',
    DO_FAQ_DETAIL: 'faq/doFaqDetail',
    DO_FAQ_UPDATE: 'faq/doFaqUpdate',
    DO_FAQ_DELETE: 'faq/doFaqDelete',
}
