export default {
    DO_USER_LIST: 'member/doUserList',
    DO_USER_DETAIL: 'member/doUserDetail',
    DO_LIST_PAGING: 'member/doListPaging',
}
