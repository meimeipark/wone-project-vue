export default {
    DO_NOTICE_LIST: 'board/doNoticeList', //get 공지사항만 조회
    DO_NOTICE_LIST_PAGING: 'faq/doNoticeListPaging', //get 공지사항 페이징
    DO_GUIDE_LIST: 'board/doGuideList', //get 이용방법만 조회
    DO_GUIDE_LIST_PAGING: 'faq/doGuideListPaging', //get 공지사항 페이징
    DO_NOTICE_DETAIL: 'board/doNoticeDetail',
    DO_CREATE_NOTICE: 'board/doCreateNotice',
    DO_UPDATE: 'board/doUpdate',

}
